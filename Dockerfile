ARG BUILD_OS=linux
ARG BUILD_ARCH=amd64
ARG BUILD_PLATFORM=linux/amd64
ARG BUILD_ARM_VERSION=''
ARG BUILD_ARCH_NAME=amd64

ARG NOMAD_VERSION=1.7.2
ARG VAULT_VERSION=1.15.4
ARG CONSUL_VERSION=1.17.1
ARG TERRAFORM_VERSION=1.6.6
ARG LEVANT_VERSION=0.3.3
ARG GITLAB_RUNNER_VERSON=16.7.0

FROM ubuntu:22.04
LABEL maintainer="steven@carboncollins.uk" \
      service=gitlab-shell-runner

ARG TIMEZONE="Europe/Stockholm"
ARG BUILD_OS
ARG BUILD_ARCH
ARG BUILD_PLATFORM
ARG BUILD_ARM_VERSION
ARG BUILD_ARCH_NAME

ARG NOMAD_VERSION
ARG VAULT_VERSION
ARG CONSUL_VERSION
ARG TERRAFORM_VERSION
ARG LEVANT_VERSION
ARG GITLAB_RUNNER_VERSON

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install --no-install-recommends --yes \
        locales \
        tzdata \
        ca-certificates \
        libcap2-bin \
        gnupg2 \
        curl \
        tar \
        lsb-release \
        git \
        git-lfs \
        unzip \
        dumb-init \
        jq \
        ldnsutils && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/*


# Set locale
RUN sed -i '/en_GB.UTF-8/s/^# //g' /etc/locale.gen && \
    sed -i '/sv_SE.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG=en_GB.UTF-8 \
    LANGUAGE=en_GB.UTF-8 \
    LC_CTYPE=en_GB.UTF-8 \
    LC_ALL=en_GB.UTF-8

# Set local time zone
RUN ln -fs /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

# Install Vault
RUN curl -L -o /tmp/vault.zip "https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_${BUILD_OS}_${BUILD_ARCH}.zip" && \
    unzip /tmp/vault.zip -d /usr/local/bin && \
    rm /tmp/vault.zip && \
    chmod +x /usr/local/bin/vault && \ 
    setcap cap_ipc_lock= /usr/local/bin/vault

# Install Consul
RUN curl -L -o /tmp/consul.zip "https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_${BUILD_OS}_${BUILD_ARCH}.zip" && \
    unzip /tmp/consul.zip -d /usr/local/bin && \
    rm /tmp/consul.zip && \
    chmod +x /usr/local/bin/consul

# Install Nomad
RUN curl -L -o /tmp/nomad.zip "https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_${BUILD_OS}_${BUILD_ARCH}.zip" && \
    unzip /tmp/nomad.zip -d /usr/local/bin && \
    rm /tmp/nomad.zip && \
    chmod +x /usr/local/bin/nomad

# Install Terraform
RUN curl -L -o /tmp/terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_${BUILD_OS}_${BUILD_ARCH}.zip" && \
    unzip /tmp/terraform.zip -d /usr/local/bin && \
    rm /tmp/terraform.zip && \
    chmod +x /usr/local/bin/terraform

# Install Levant
RUN curl -L -o /tmp/levant.zip "https://releases.hashicorp.com/levant/${LEVANT_VERSION}/levant_${LEVANT_VERSION}_${BUILD_OS}_${BUILD_ARCH}.zip" && \
    unzip /tmp/levant.zip -d /usr/local/bin/ && \
    rm /tmp/levant.zip
RUN chmod +x /usr/local/bin/levant

# Init Git LFS
RUN git lfs install --system

# Install gitlab runner
RUN curl -L -o /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/v${GITLAB_RUNNER_VERSON}/binaries/gitlab-runner-${BUILD_OS}-${BUILD_ARCH}" && \
    chmod +x /usr/local/bin/gitlab-runner

RUN useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

RUN mkdir -p /home/gitlab-runner/workspace && \
    chown gitlab-runner /home/gitlab-runner/workspace

COPY ./entrypoints /entrypoints/
RUN chmod +x /entrypoints/*.sh

STOPSIGNAL SIGTERM

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

#!/usr/bin/env bash

set -ueo pipefail

/usr/local/bin/gitlab-runner run \
  --config "${CONFIG_FILE}" \
  --working-directory="/home/gitlab-runner/workspace" \
  --user "gitlab-runner" \
  --listen-address ":${NOMAD_PORT_metrics}"

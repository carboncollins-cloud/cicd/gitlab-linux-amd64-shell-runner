#!/usr/bin/env bash

set -ueo pipefail

/usr/local/bin/gitlab-runner register \
  --non-interactive \
  --url "${CI_SERVER_URL}" \
  --registration-token "${REGISTRATION_TOKEN}" \
  --config "${CONFIG_FILE}" \
  --executor "shell" \
  --shell "bash" \
  --name "${RUNNER_NAME}" \
  --tag-list "${NOMAD_DC},${NOMAD_REGION},${RUNNER_PLATFORM},nomad,terraform,consul,vault,levant,jq,shell"

if [ $? -gt 0 ]; then
  echo "Failed to register with gitlab"
  exit 1
fi

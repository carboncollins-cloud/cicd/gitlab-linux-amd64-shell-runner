job "cicd-gitlab-linux-amd64-shell-runner" {
  name        = "GitLab Runner (Linux AMD64 Shell)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-cicd-gitlab"

  group "linux-amd64" {
    count = 1

    spread {
      attribute = "${node.node.unique.name}"
      weight = 50
    }

    constraint {
      attribute = "${attr.cpu.arch}"
      value = "amd64"
    }

    network {
      port "metrics" { to = 9252 }
    }

    service {
      name = "gitlab-shell-runner"
      port = "${NOMAD_PORT_metrics}"
      task = "runner"
    }

    task "register" {
      driver = "docker"

      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      config {
        image = "[[ .jobDockerImage ]]"

        args = ["bash", "-c", "/entrypoints/register.sh"]
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        CONFIG_FILE = "${NOMAD_ALLOC_DIR}/gitlab-runner-config.toml"
        CI_SERVER_URL = "https://gitlab.com/"

        RUNNER_NAME = "${NOMAD_ALLOC_ID}"
        RUNNER_PLATFORM = "linux-amd64"
      }
    }

    task "runner" {
      driver = "docker"

      kill_timeout = "60s"

      config {
        image = "[[ .jobDockerImage ]]"

        ports = ["metrics"]

        args = ["bash", "-c", "/entrypoints/runner.sh"]
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        CONFIG_FILE = "${NOMAD_ALLOC_DIR}/gitlab-runner-config.toml"
        CI_SERVER_URL = "https://gitlab.com/"

        RUNNER_NAME = "${NOMAD_ALLOC_ID}"
        RUNNER_PLATFORM = "linux-amd64"

        NOMAD_NAMESPACE = "cicd-gitlab"
        NOMAD_REGION = "se"
        NOMAD_ADDR = "[[ .nomadAddress ]]"
        // for now

        VAULT_ADDR = "[[ .vaultAddress ]]"
        VAULT_SKIP_VERIFY = "true"
        // for now

        CONSUL_HTTP_ADDR = "[[ .consulAddress ]]"
        CONSUL_HTTP_SSL = "true"
        CONSUL_HTTP_SSL_VERIFY = "false"
        // for now
      }
    }

    task "unregister" {
      driver = "docker"

      lifecycle {
        hook = "poststop"
      }

      config {
        image = "[[ .jobDockerImage ]]"

        args = ["bash", "-c", "/entrypoints/unregister.sh"]
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        CONFIG_FILE = "${NOMAD_ALLOC_DIR}/gitlab-runner-config.toml"
        CI_SERVER_URL = "https://gitlab.com/"

        RUNNER_NAME = "${NOMAD_ALLOC_ID}"
        RUNNER_PLATFORM = "linux-amd64"
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert       = true
    auto_promote      = true
    stagger = "30s"
    canary = 1
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
